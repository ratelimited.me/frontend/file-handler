template_names = {
    "image": "image.jinja.html",
    "video": "video.jinja.html",
    "audio": "audio.jinja.html",
    "other": "non_embeddable.jinja.html",
}

file_types = {
    "image": ["png", "bmp", "jpg", "jpeg", "gif",],
    "video": ["mp4", "avi", "webm",],
    "audio": ["mp3", "ogg", "wav", "flac",],
}
