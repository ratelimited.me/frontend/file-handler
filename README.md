## RATELIMITED File Handler 2.0
_Now in python!_

This is the second revision of the File Handler powering RATELIMITED's file delivery frontend.

### Setting up
It's fairly simple, copy `.env.example` to `.env`, modify the values, and use it with RLAPI v3 (or v4, if that ever even gets finished lol)

**_notice: database modifications are required, please check the db modification section_**


### Database Modifications

To run this, you'll have to run the two following postgres commands:
`ALTER TABLE users ADD COLUMN embed_method text;`, and `ALTER TABLE files ADD COLUMN description text;`.