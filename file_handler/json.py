from quart import current_app as app
import json


async def get_info(id):
    json_info = {}

    json_info["exists"] = await exists(id)

    if json_info["exists"]:
        json_info["data"] = json.loads(await get(id))

    return json_info


async def exists(id):
    if app.db.fetchval("SELECT id FROM json_uploads WHERE id = $1", id) is not None:
        return True

    return False


async def get(id):
    return app.db.fetchval("SELECT json FROM json_uploads WHERE id = $1", id)
