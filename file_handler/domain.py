import os
import tldextract
from quart import current_app as app


async def is_authorized(url):
    domain = tldextract.extract(url)

    if (
        await app.db.fetchval(
            "SELECT domain_name FROM domains WHERE domain_name = $1 OR domain_name = $2 OR domain_name = $3 AND verified = true",
            domain.registered_domain,
            domain.fqdn,
            domain.domain,
        )
        is not None
    ):
        return True

    return False


async def get_options(url):
    domain = tldextract.extract(url)
    domain_options = {
        "has_custom_bucket": False,
    }

    if await is_authorized(url):
        domain_options["bucket"] = await app.db.fetchval(
            "SELECT bucket FROM domains WHERE domain_name = $1 OR domain_name = $2 OR domain_name = $3 AND verified = true",
            domain.registered_domain,
            domain.fqdn,
            domain.domain,
        )

        if domain_options["bucket"] != os.getenv("DEFAULT_BUCKET"):
            domain_options["has_custom_bucket"] = True

    return domain_options
