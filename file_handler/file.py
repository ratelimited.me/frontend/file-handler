from quart import current_app as app
import os
import datetime
from configs import file_types


async def get_info(file_name):
    file_info = {}
    file_info["exists"] = await exists(file_name)

    if file_info["exists"]:
        file_meta = await get_meta(file_name)

        file_info["upload_date"] = datetime.datetime.fromtimestamp(
            int(file_meta["timestamp"])
        ).strftime("%Y-%m-%d")

        file_info["upload_time"] = datetime.datetime.fromtimestamp(
            int(file_meta["timestamp"])
        ).strftime("%H:%M:%S")

        file_info["original_filename"] = file_meta["originalfilename"]

    return file_info


async def exists(file_name):
    if (
        await app.db.fetchval(
            "SELECT filename FROM files WHERE filename = $1", file_name
        )
    ) is not None:
        return True

    return False


async def get_meta(file_name):
    file_meta = await app.db.fetchrow(
        "SELECT originalfilename, timestamp, description FROM files WHERE filename = $1",
        file_name,
    )

    return file_meta


async def get_template_type(url):
    file_extension = url.split(".")[-1]

    if file_extension in file_types["image"]:
        return "image"
    elif file_extension in file_types["video"]:
        return "video"
    elif file_extension in file_types["audio"]:
        return "audio"
    else:
        return "other"
