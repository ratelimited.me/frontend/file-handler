from quart import current_app as app


async def file_owner_options(file_name):
    owner_settings = {"embed_method": "full"}

    db_result = await app.db.fetchval(
        "SELECT embed_method FROM users WHERE id = (SELECT user_id FROM files WHERE filename = $1)",
        file_name,
    )

    if db_result is not None:
        owner_settings["embed_method"] = db_result

    return owner_settings


async def change_embed_method(api_key, embed_method):
    if await api_key_exists(api_key):
        if embed_method == "full":
            return await set_embed_method(api_key, "full")
        elif embed_method == "dsc_bypass":
            return await set_embed_method(api_key, "dsc_bypass")
        else:
            return await set_embed_method(api_key, "none")
    else:
        return {"success": False, "message": "Invalid Authentication"}


async def api_key_exists(api_key):
    if (
        app.db.fetchval("SELECT token FROM tokens WHERE token = $1", api_key)
        is not None
    ):
        return True

    return False


async def set_embed_method(api_key, embed_method):
    await app.db.execute(
        "UPDATE users SET embed_method = $1 WHERE id = (SELECT user_id FROM tokens WHERE token = $2)",
        embed_method,
        api_key,
    )

    return {
        "success": True,
        "message": "Your embedding method has been updated to: " + embed_method,
    }