from quart import current_app as app
import datetime


async def get_info(id):
    id = "~." + id  # Honestly, I'm stupid. Why did I add ~. into the db?

    shorten_info = {}

    shorten_info["exists"] = await exists(id)

    if shorten_info["exists"]:
        short_meta = await get_meta(id)

        shorten_info["url"] = short_meta["url"]

        shorten_info["url_safe"] = short_meta["url_safe"]

        shorten_info["upload_date"] = datetime.datetime.fromtimestamp(
            int(short_meta["timestamp"])
        ).strftime("%Y-%m-%d")

        shorten_info["upload_time"] = datetime.datetime.fromtimestamp(
            int(short_meta["timestamp"])
        ).strftime("%H:%M:%S")
    return shorten_info


async def exists(id):
    if (
        app.db.fetchval(
            "SELECT short_name FROM shortened_urls WHERE short_name = $1", id
        )
        is not None
    ):
        return True

    return False


async def get_meta(id):
    return await app.db.fetchrow(
        "SELECT url, url_safe, timestamp FROM shortened_urls WHERE short_name = $1",
        id,
    )
