# vv Only import if you've got a Sqreen account
# import sqreen
import requests
import sentry_sdk
import os
from dotenv import load_dotenv
from quart import (
    Quart,
    request,
    Response,
    render_template,
    jsonify,
    redirect,
)
import asyncpg as db
import file_handler
from file_handler import (
    user as user_f,
    json as json_f,
    domain as domain_f,
    shorten as shorten_f,
    file as file_f,
)

from sentry_sdk import capture_exception
from configs import template_names

# vv Only uncomment if you've got a Sqreen account
# sqreen.start()
load_dotenv()
app = Quart(__name__)

if os.getenv("SENTRY_ENABLED"):
    sentry_sdk.init(dsn=os.getenv("SENTRY_DSN"))

# Disable JSONify module from re-arranging our responses
app.config["JSON_SORT_KEYS"] = False


@app.before_serving
async def app_before_serving():
    app.db = await db.create_pool(
        host=os.getenv("DB_HOST"),
        port=os.getenv("DB_PORT"),
        database=os.getenv("DB_NAME"),
        user=os.getenv("DB_USER"),
        password=os.getenv("DB_PASS"),
        min_size=30,
        max_size=50,
        max_inactive_connection_lifetime=15,
    )


@app.errorhandler(500)
def handle_exception(exception):
    """Handle any kind of exception."""
    status_code = 500

    try:
        status_code = exception.status_code
    except AttributeError:
        pass

    capture_exception(exception)
    return (
        jsonify(
            {
                "success": False,
                "message": "There was an error, please let a system administrator know.",
            }
        ),
        status_code,
    )


@app.route("/")
async def index():
    return jsonify(
        success=True,
        message="The File Handler is working normally.",
        node_info={"version": 2},
    )


@app.route("/favicon.ico")
async def getFavicon():
    return jsonify({"success": False, "message": "i was too lazy to include a favicon"})


@app.route("/updateMethod.rlme_int")
async def updateMethod():
    apiKey = request.args.get("key")
    newBehavior = request.args.get("method")

    if apiKey is not None and newBehavior is not None:
        return await user_f.change_embed_method(apiKey, newBehavior)
    else:
        return jsonify(
            {"success": False, "message": "Did not contain the correct parameters"}
        )


# JSON hosting Service // To be deprecated
@app.route("/~json.<jsonId>")
async def getJson(jsonId):
    if not (await domain_f.is_authorized(request.url)):
        return jsonify(
            success=False,
            message="This domain is not authorized for use with %s services. Please authorize it to begin use."
            % os.getenv("SERVICE_NAME"),
        )

    json = await json_f.get_info(jsonId)

    if json["exists"]:
        return jsonify(json["data"])
    else:
        return await render_template("404.html")


# Link / URL Shortening
@app.route("/~.<shortId>")
async def getShort(shortId):
    if not (await domain_f.is_authorized(request.url)):
        return jsonify(
            success=False,
            message="This domain is not authorized for use with %s services. Please authorize it to begin use."
            % os.getenv("SERVICE_NAME"),
        )

    short = await shorten_f.get_info(shortId)

    if short["exists"]:
        if short["url_safe"]:
            return redirect(short["url"])
        else:
            return await render_template(
                "short_url_unsafe.jinja.html",
                service_name=os.getenv("SERVICE_NAME"),
                report_mailto_url=os.getenv("REPORTING_MAILTO"),
                original_url=short["url"],
                date_date=short["upload_date"],
                date_time=short["upload_time"],
            )
    else:
        return await render_template("404.html")


# Files
@app.route("/<filename>")
async def getFile(filename):
    if not (await domain_f.is_authorized(request.url)):
        return jsonify(
            success=False,
            message="This domain is not authorized for use with %s services. Please authorize it to begin use."
            % os.getenv("SERVICE_NAME"),
        )

    file = await file_f.get_info(filename)

    if file["exists"]:
        userOptions = await user_f.file_owner_options(filename)
        domainOptions = await domain_f.get_options(request.url)

        if userOptions["embed_method"] == "full":
            if domainOptions["has_custom_bucket"]:
                bucket = domainOptions["bucket"]
                raw_url = os.getenv("RAW_CB_CDN_URL") + bucket + "/" + filename
            else:
                raw_url = os.getenv("RAW_CDN_URL") + "/" + filename

            return await render_template(
                template_names[await file_f.get_template_type(request.base_url)],
                service_name=os.getenv("SERVICE_NAME"),
                report_mailto_url=os.getenv("REPORTING_MAILTO"),
                raw_url=raw_url,
                original_filename=file["original_filename"],
                file_description="",
                date_date=file["upload_date"],
                date_time=file["upload_time"],
            )

        elif userOptions[
            "embed_method"
        ] == "dsc_bypass" and "Discordbot" in request.headers.get("User-Agent"):
            if domainOptions["has_custom_bucket"]:
                bucket = domainOptions["bucket"]
                raw_url = os.getenv("RAW_CB_CDN_URL") + bucket + "/" + filename
            else:
                raw_url = os.getenv("RAW_CDN_URL") + "/" + filename

            return await render_template(
                "dsc_bypass_"
                + template_names[await file_f.get_template_type(request.base_url)],
                raw_url=raw_url,
                original_filename=file["original_filename"],
            )

        else:
            if domainOptions["has_custom_bucket"]:
                bucket = domainOptions["bucket"]
                raw_url = os.getenv("INT_RAW_CB_CDN_URL") + bucket + "/" + filename
            else:
                raw_url = (
                    os.getenv("INT_RAW_CDN_URL")
                    + os.getenv("DEFAULT_BUCKET")
                    + "/"
                    + filename
                )

            try:
                req = requests.get(raw_url, stream=True)

                return Response(
                    req.iter_content(chunk_size=1024),
                    content_type=req.headers["content-type"],
                )
            except Exception as e:
                handle_exception(e)

    else:
        return await render_template("404.html")


if __name__ == "__main__":
    app.run(port=8080)
